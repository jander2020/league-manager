﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace LeagueManager.Models
{
    public class YouthPlayer
    {
        [Key]
        public int PlayerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ParentID { get; set; }
        public bool Soccer { get; set; }
        public bool Baseball { get; set; }
        public bool Basketball { get; set; }
        public bool Softball { get; set; }
        public bool Cheerleading { get; set; }
        public bool Football { get; set; }
    }
    public class PlayerDbContext:DbContext
    {
        public DbSet<YouthPlayer> Players { get; set; }
    }
}
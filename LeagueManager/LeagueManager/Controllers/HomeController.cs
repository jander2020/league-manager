﻿using System.Web.Mvc;

namespace LeagueManager.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }
        public ActionResult BaseBall()
        {
            ViewBag.Message = "Baseball landing page.";

            return View();
        }
        public ActionResult BasketBall()
        {
            ViewBag.Message = "Basketball landing page.";

            return View();
        }
        public ActionResult CheerLeading()
        {
            ViewBag.Message = "Cheerleading landing page.";

            return View();
        }
        public ActionResult FootBall()
        {
            ViewBag.Message = "Football landing page.";

            return View();
        }
        public ActionResult Soccer()
        {
            ViewBag.Message = "Soccer landing page.";

            return View();
        }
        public ActionResult SoftBall()
        {
            ViewBag.Message = "Softball landing page.";

            return View();
        }
        public ActionResult PlayerAdd()
        {
            return View();
        }
        public ActionResult AssignPlayers()
        {
            return View();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LeagueManager.Models;

namespace LeagueManager.Controllers
{
    public class YouthPlayersController : Controller
    {
        private PlayerDbContext db = new PlayerDbContext();

        // GET: YouthPlayers
        public ActionResult Index()
        {
            return View(db.Players.ToList());
        }

        // GET: YouthPlayers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            YouthPlayer youthPlayer = db.Players.Find(id);
            if (youthPlayer == null)
            {
                return HttpNotFound();
            }
            return View(youthPlayer);
        }

        // GET: YouthPlayers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: YouthPlayers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PlayerID,FirstName,LastName,ParentID,Soccer,Baseball,Basketball,Softball,Cheerleading,Football")] YouthPlayer youthPlayer)
        {
            if (ModelState.IsValid)
            {
                db.Players.Add(youthPlayer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(youthPlayer);
        }

        // GET: YouthPlayers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            YouthPlayer youthPlayer = db.Players.Find(id);
            if (youthPlayer == null)
            {
                return HttpNotFound();
            }
            return View(youthPlayer);
        }

        // POST: YouthPlayers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PlayerID,FirstName,LastName,ParentID,Soccer,Baseball,Basketball,Softball,Cheerleading,Football")] YouthPlayer youthPlayer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(youthPlayer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(youthPlayer);
        }

        // GET: YouthPlayers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            YouthPlayer youthPlayer = db.Players.Find(id);
            if (youthPlayer == null)
            {
                return HttpNotFound();
            }
            return View(youthPlayer);
        }

        // POST: YouthPlayers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            YouthPlayer youthPlayer = db.Players.Find(id);
            db.Players.Remove(youthPlayer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
